## Mô hình MVC là gì?
- MVC - “Model-View-Controller“. MVC là một kiểu kiến trúc lập trình phần mềm có ba phần được kết nối với nhau và mỗi thành phần đều có một nhiệm vụ riêng của nó và độc lập với các thành phần khá̀n:
    - Model (dữ liệu): Quản lí xử lí các dữ liệu.
    - View (giao diện): Nới hiển thị dữ liệu cho người dùng.
    - Controller (bộ điều khiển): Điều khiển sự tương tác của hai thành phần Model và View.

## Luồng xử lý trong MVC
- Khi một yêu cầu của từ máy khách (Client) gửi đến Server, controller tương ứng sẽ được gọi để xử lý yêu cầu.
- Sau đó, Controller xử lý input của user rồi giao tiếp với Model trong MVC.
- Model chuẩn bị data và gửi lại cho Controller.
- Cuối cùng, khi xử lý xong yêu cầu thì Controller gửi dữ liệu trở lại View và hiển thị cho người dùng trên trình duyệt.

## Ưu điểm của MVC
- Ta có thể dễ dàng bảo trì ứng dụng
- Khả năng cung cấp nhiều chế độ view
- Các sửa đổi không ảnh hưởng đến toàn bộ mô hình
