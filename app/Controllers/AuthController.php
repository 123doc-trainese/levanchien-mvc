<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Core\View;
use App\Models\User;

class AuthController extends Controller
{
    public function displayLoginForm()
    {
        if (isset($_SESSION['status'])) {
            if ($_SESSION['role'] == 0) {
                header('location: /showInfor');
            } else {
                header('location: /');
            }
            return;
        }
        View::render('login_form.php');
    }

    public function displayRegisterForm()
    {
        if (isset($_SESSION['status'])) {
            if ($_SESSION['role'] == 0) {
                header('location: /showInfor');
            } else {
                header('location: /');
            }
            return;
        }
        View::render('register_form.php');
    }

    public function login()
    {
        if (isset($_SESSION['status'])) {
            if (isset($_SESSION['status'])) {
                if ($_SESSION['role'] == 0) {
                    header('location: /showInfor');
                } else {
                    header('location: /');
                }
                return;
            }
        }
        $username = $_POST['username'];
        $password = $_POST['password'];
        $user = User::checkLogin($username, $password);
        if (!empty($user)) {
            $_SESSION['status'] = 1;
            $_SESSION['id'] = $user['id'];
            $_SESSION['role'] = $user['role'];
            if ($_SESSION['role'] === 1) {
                header('location: /');
            } else {
                header('location: /showInfor');
            }
        } else {
            $error = 'Username or Password is not exactly';
            View::render('login_form.php', compact('error'));
        }
    }

    public function register()
    {
        if (isset($_SESSION['status'])) {
            if (isset($_SESSION['status'])) {
                if ($_SESSION['role'] == 0) {
                    header('location: /showInfor');
                } else {
                    header('location: /');
                }
                return;
            }
        }

        $user['username'] = $_POST['username'];
        $user['password'] = $_POST['password'];
        $password_confirm = $_POST['password_confirm'];
        if ($user['password'] !== $password_confirm) {
            $error = 'Password và Password_Confirm không giỐng nhau';
            View::render('register_form.php', compact('error'));
            return;
        }

        if (User::checkUsername($user['username']) === true) {
            $error = 'Username đã tồn tại';
            View::render('register_form.php', compact('error'));
            return;
        };

        $user['role'] = 0;
        User::insert($user);
        $user = User::checkLogin($user['username'], $user['password']);

        if (!empty($user)) {
            $_SESSION['status'] = 1;
            $_SESSION['id'] = $user['id'];
            $_SESSION['role'] = $user['role'];
            if ($_SESSION['role'] === 1) {
                header('location: /');
            } else {
                header('location: /showInfor');
            }
        } else {
            $error = 'Username or Password is not exactly';
            View::render('login_form.php', compact('error'));
        }
    }


    public function logout()
    {
        session_destroy();
        header('location: /login-form');
    }
}
