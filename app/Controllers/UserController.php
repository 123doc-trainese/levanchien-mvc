<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Core\View;
use App\Models\User;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
        View::render('welcome.php', compact('users'));
    }

    public function showInfor()
    {
        $user = User::get($_SESSION['id']);
        View::render('user/infor.php', compact('user'));
    }
    public function displayAddForm()
    {
        View::render('admin/add_form.php');
    }

    public function insert()
    {
        $new_user['username'] = $_POST['username'];
        $new_user['password'] = $_POST['password'];
        $new_user['role'] = $_POST['role'];
        User::insert($new_user);
        header('localtion: /');
    }

    public function delete($id)
    {
        User::delete($id);
        header('location: /');
    }

    public function displayUpdateForm($id)
    {
        if ($_SESSION['role'] === 0) {
            $user = User::get($_SESSION['id']);
        } else {
            $user = User::get($id);
        }
        View::render('update_form.php', compact('user'));
    }
    public function update()
    {
        $user['id'] = $_POST['id'];
        $user['username'] = $_POST['username'];
        $user['password'] = $_POST['password'];
        if ($_SESSION['role'] === 0) {
            $user['id'] == $_SESSION['id'];
            $user['role'] = 0;
            User::update($user);
            header('location: /showInfor');
        } else {
            $user['role'] = $_POST['role'];
            User::update($user);
            header('location: /');
        }
    }
}
