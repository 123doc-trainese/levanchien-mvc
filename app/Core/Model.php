<?php

namespace App\Core;

use \PDO;

abstract class Model
{
    protected static $table = '';
    protected static $attributes = [];
    /**
     * Get the PDO database connection
     *
     * @return \PDO|null
     */
    protected static function getDB()
    {
        static $db = null;

        if ($db === null) {
            $dsn = 'mysql:host=' . config('db_host') . '; port=' . config('db_port') . ';dbname=' . config('db_database') . ';charset=utf8';
            $db = new PDO($dsn, config('db_username'), config('db_password'));

            // Throw an Exception when an error occurs
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $db;
    }

    public static function all()
    {
        $db = static::getDB();
        $table_name = static::$table;
        $stmt = $db->query("SELECT * FROM $table_name");
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function get($id)
    {
        $db = static::getDB();
        $table_name = static::$table;
        $item = $db->query("SELECT * FROM $table_name WHERE `id` = $id");
        return $item->fetch(\PDO::FETCH_ASSOC);
    }

    public static function insert($data)
    {
        $db = static::getDB();
        $table_name = static::$table;
        $attributes = implode(',', static::$attributes);
        $username = $data['username'];
        $password = $data['password'];
        $role = $data['role'];
        $result = $db->query("INSERT INTO $table_name ($attributes) VALUES ('$username', '$password', '$role')");
        return $result;
    }

    /**
     * Delete a record
     *
     * @param $id
     * @return bool
     */
    public static function delete($id)
    {
        $db = static::getDB();
        $table_name = static::$table;
        $sql = "DELETE FROM $table_name WHERE id = :id";
        $stmt = $db->prepare($sql);
        return $stmt->execute(array(':id' => $id));
    }

    public static function update($user)
    {
        $db = static::getDB();
        $table_name = static::$table;
        $id = $user['id'];
        $username = $user['username'];
        $password = $user['password'];
        $role = $user['role'];
        $set = array_map(function ($key, $value) {
            return "$key = '$value'";
        }, array_keys($user), array_values($user));
        $set = implode(', ', $set);
        $sql = "UPDATE $table_name SET $set WHERE `id` = $id";
        $result = $db->query($sql);
        return $result;
    }
}
