<?php

namespace App\Core;

class View
{
    /**
     * Render a view file
     *
     * @param string $view The view file
     * @param array $args Associative array of data to display in the view (optional)
     *
     * @return bool
     * @throws \Exception
     */
    public static function render(string $view, array $args = [])
    {
        View::include('inc/header.php');

        extract($args, EXTR_SKIP); //EXTR_SKIP - On collision, the existing variable is not overwritten

        $file = dirname(__DIR__) . "/Views/$view";  // relative to Core directory

        if (is_readable($file)) {
            require $file;
        } else {
            throw new \Exception("$file not found");
        }

        View::include('inc/footer.php');
    }

    /**
     * Include a view file
     *
     * @param string $view The view file
     *
     * @return bool
     * @throws \Exception
     */
    public static function include(string $view)
    {
        $file = dirname(__DIR__) . "/Views/$view";  // relative to Core directory
        if (is_readable($file)) {
            require_once $file;
        } else {
            throw new \Exception("$file not found");
        }
    }
}
