<?php

namespace App\Models;

use App\Core\Model;

class User extends Model
{
    protected static $table = 'users';
    protected static $attributes = ['username', 'password', 'role'];

    public static function checkLogin($username, $password)
    {
        $db = static::getDB();
        $sql = "SELECT * FROM `users` WHERE `username` = '$username' AND `password` = '$password'";
        $result = $db->query($sql);
        return $result->fetch(\PDO::FETCH_ASSOC);
    }

    public static function checkUsername($username)
    {
        $db = static::getDB();
        $sql = "SELECT * FROM `users` WHERE `username` = '$username'";
        $result = $db->query($sql);
        return !empty($result->fetch(\PDO::FETCH_ASSOC));
    }
}
