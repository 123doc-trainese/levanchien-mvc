<h2>Register Form</h2>
<form action="/register" method="post">
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Username</label>
        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="username">
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" name="password">
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Password Confirm</label>
        <input type="password" class="form-control" id="exampleInputPassword1" name="password_confirm">
    </div>
    <div class="mb-3">
        <p style="color:red"><?php echo $error ?? "" ?></p>
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
</form>
<a href="/login-form">
    <button type="submit" class="btn btn-secondary">Login</button>
</a>