<form action="/update" method="post">
    <div class="form-group">
        <input type="hidden" class="form-control" name="id" value=<?php echo $user['id'] ?>>
    </div>
    <div class="form-group">
        <label>UserName</label>
        <input type="text" class="form-control" name="username" value=<?php echo $user['username'] ?>>
    </div>
    <div class="form-group">
        <label>Password</label>
        <input type="text" class="form-control" name="password" value=<?php echo $user['password'] ?>>
    </div>
    <?php if ($_SESSION['role'] == 1) : ?>
        <div class="form-group">
            <label>Role</label>
            <input type="text" class="form-control" name="role" value=<?php echo $user['role'] ?>>
        </div>
    <?php endif; ?>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
<a href="/">
    <button class="btn btn-secondary">Back</button>
</a>