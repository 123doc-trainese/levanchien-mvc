<a href="/logout">
    <button type="button" class="btn btn-dark">Logout</button>
</a>
<a href="/add-form">
    <button type="button" class="btn btn-primary">Add New</button>
</a>
<table class="table">
    <thead>
        <tr>
            <th>id</th>
            <th>username</th>
            <th>password</th>
            <th>role</th>
            <th>action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?php echo $user['id'] ?></td>
                <td><?php echo $user['username'] ?></td>
                <td><?php echo $user['password'] ?></td>
                <td><?php echo $user['role'] ?></td>
                <td>
                    <a href="/update-form/<?php echo $user['id'] ?>">
                        <button type="button" class="btn btn-secondary">Update</button>
                    </a>
                    <a href="/delete/<?php echo $user['id'] ?>">
                        <button type="button" class="btn btn-dark">Delete</button>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>