<?php

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';
/**
 * Load Env
 */
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('\App\Core\Error::errorHandler');
set_exception_handler('\App\Core\Error::exceptionHandler');

if (session_id() === '') session_start();
/**
 * Routing
 */
$router = new \App\Core\Router();

// Add the routes
$router->add('/login-form', [
    'controller' => 'AuthController',
    'action' => 'displayLoginForm'
]);
$router->add('/login', [
    'controller' => 'AuthController',
    'action' => 'login'
]);
$router->add('/register-form', [
    'controller' => 'AuthController',
    'action' => 'displayRegisterForm'
]);
$router->add('/register', [
    'controller' => 'AuthController',
    'action' => 'register'
]);
if (isset($_SESSION['status'])) {
    $router->add('/showinfor', [
        'controller' => 'UserController',
        'action' => 'showInfor'
    ]);
    $router->add('/logout', [
        'controller' => 'AuthController',
        'action' => 'logout'
    ]);
    $router->add('/update-form/{id}', [
        'controller' => 'UserController',
        'action' => 'displayUpdateForm'
    ]);
    $router->add('/update', [
        'controller' => 'UserController',
        'action' => 'update'
    ]);
}

if (isset($_SESSION['role']) && $_SESSION['role'] == 1) {
    $router->add('/', [
        'controller' => 'UserController',
        'action' => 'index'
    ]);
    $router->add('/add-form', [
        'controller' => 'UserController',
        'action' => 'displayAddForm'
    ]);
    $router->add('/insert', [
        'controller' => 'UserController',
        'action' => 'insert'
    ]);
    $router->add('/delete/{id}', [
        'controller' => 'UserController',
        'action' => 'delete'
    ]);
}
$router->dispatch($_SERVER['REQUEST_URI']);
// print_r($_SESSION);
